/*
 * Copyright 2017 Urs Fässler
 * SPDX-License-Identifier: Apache-2.0
 */

package ch.bitzgi.android.bluetooth.demo.adapter;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.android.bluetooth.adapter.ActivityAdapter;

public class BluetoothAdapterEventGenerator {
    private final ActivityAdapter adapter;
    private final BluetoothAdapterEventListener handler;
    private boolean turnedOn = false;

    public BluetoothAdapterEventGenerator(int bluetoothEnableCode, Activity activity, BluetoothAdapterEventListener handler) {
        adapter = new ActivityAdapter(bluetoothEnableCode, activity);
        this.handler = handler;
    }

    public void turnOn() {
        turnedOn = false;
        adapter.enable();
    }

    public void calc() {
        boolean newTurnedOn = adapter.isEnabled();
        if (newTurnedOn & !turnedOn) {
            handler.turnedOn();
        }
        turnedOn = newTurnedOn;
    }

    public List<BluetoothDevice> getDevices() {
        return new ArrayList<BluetoothDevice>(adapter.devices());
    }


}

