/*
 * Copyright 2018 Urs Fässler
 * SPDX-License-Identifier: Apache-2.0
 */

package ch.bitzgi.android.bluetooth.demo.app;

public enum MessageId {
    REQUEST_ENABLE_BT,
    TICK,
}
