/*
 * Copyright 2018 Urs Fässler
 * SPDX-License-Identifier: Apache-2.0
 */

package ch.bitzgi.android.bluetooth.demo.tick;

import android.os.Handler;
import android.os.Message;


public class Ticker extends Handler {
    private static final int Interval = 200;
    private final int code;
    private final TickHandler handler;

    public Ticker(int code, TickHandler handler) {
        this.code = code;
        this.handler = handler;
    }

    public void start() {
        sendEmptyMessageDelayed(code, Interval);
    }

    @Override
    public void handleMessage(Message msg) {
        if (msg.what == code) {
            sendEmptyMessageDelayed(code, Interval);
            handler.tick();
        }
    }
}
