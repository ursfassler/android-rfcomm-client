/*
 * Copyright 2018 Urs Fässler
 * SPDX-License-Identifier: Apache-2.0
 */

package ch.bitzgi.android.bluetooth.demo.app;

import android.bluetooth.BluetoothDevice;

class GuiDevice {
    public final BluetoothDevice device;

    GuiDevice(BluetoothDevice device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return device.getName() + "\n" + device.getAddress();
    }
}
