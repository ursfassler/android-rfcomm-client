/*
 * Copyright 2018 Urs Fässler
 * SPDX-License-Identifier: Apache-2.0
 */

package ch.bitzgi.android.bluetooth.demo.adapter;

public interface BluetoothAdapterEventListener {
    public void turnedOn();

}
