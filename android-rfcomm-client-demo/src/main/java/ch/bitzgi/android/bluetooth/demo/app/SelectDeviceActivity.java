/*
 * Copyright 2018 Urs Fässler
 * SPDX-License-Identifier: Apache-2.0
 */

package ch.bitzgi.android.bluetooth.demo.app;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ch.bitzgi.android.bluetooth.demo.R;
import ch.bitzgi.android.bluetooth.demo.tick.TickHandler;
import ch.bitzgi.android.bluetooth.demo.tick.Ticker;
import ch.bitzgi.android.bluetooth.spp.Output;
import ch.bitzgi.android.bluetooth.spp.Supervisor;
import ch.bitzgi.android.bluetooth.demo.adapter.BluetoothAdapterEventGenerator;
import ch.bitzgi.android.bluetooth.demo.adapter.BluetoothAdapterEventListener;

public class SelectDeviceActivity extends AppCompatActivity implements Output, TickHandler, BluetoothAdapterEventListener {
    private ArrayAdapter<String> logAdapter = null;
    final private Ticker ticker = new Ticker(MessageId.TICK.ordinal(), this);
    final private BluetoothAdapterEventGenerator adapter = new BluetoothAdapterEventGenerator(MessageId.REQUEST_ENABLE_BT.ordinal(), this, this);
    final private Supervisor rfcommServer = new Supervisor(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_device_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        logAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        ListView listView = findViewById(R.id.connection_log);
        listView.setAdapter(logAdapter);

        Button sendButton = findViewById(R.id.send);
        sendButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String sendText = getSendText();
                        List<Byte> data = parseHex(sendText);
                        logOutput("send: " + hexString(data));
                        rfcommServer.send(data);
                    }
                }
        );

        Button disconnectButton = findViewById(R.id.disconnect);
        disconnectButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rfcommServer.disconnect();
                    }
                }
        );


        ticker.start();

        adapter.turnOn();
    }

    private String getSendText() {
        EditText sendMessage = findViewById(R.id.send_message);
        String sendText = sendMessage.getText().toString();
        return sendText;
    }

    private List<Byte> parseHex(String value) {
        List<Byte> result = new ArrayList<>();

        String[] parts = value.split(" ");
        for (String part : parts) {
            int sym = Integer.parseInt(part, 16);
            result.add((byte)sym);
        }

        return result;
    }

    private void logOutput(String message) {
        logAdapter.insert(message, 0);
    }

    private void logInput(String message) {
        logAdapter.insert(message, 0);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        adapter.turnOn();
    }

    @Override
    public void turnedOn() {
        List<GuiDevice> data = new ArrayList<>();
        for (BluetoothDevice device : adapter.getDevices()) {
            data.add(new GuiDevice(device));
        }

        ArrayAdapter<GuiDevice> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data);

        ListView listView = (ListView) findViewById(R.id.devices_list);
        listView.setAdapter(adapter);

        AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                GuiDevice device = (GuiDevice) parent.getItemAtPosition(position);

                logOutput("connect to " + device.device.getAddress());

                rfcommServer.connect(device.device);
            }
        };

        listView.setOnItemClickListener(mMessageClickedHandler);
    }

    @Override
    public void disconnected() {
        logInput("disconnected");
    }

    @Override
    public void connecting(String message) {
        logInput("connecting: " + message);
    }

    @Override
    public void error(String message) {
        logInput("error: " + message);
    }

    @Override
    public void connected() {
        logInput("connected");
    }

    @Override
    public void received(List<Byte> data) {
        logInput("received: " + hexString(data));
    }

    private String hexString(Collection<Byte> data) {
        String result = "";
        boolean first = true;
        for (Byte itr : data) {
            if (first) {
                first = false;
            } else {
                result += " ";
            }
            result += hexString(itr);
        }
        return result;
    }

    private String hexString(Byte value) {
        int upper = (value >> 4) & 0x0f;
        int lower = (value >> 0) & 0x0f;
        return Integer.toHexString(upper) + Integer.toHexString(lower);
    }

    @Override
    public void tick() {
        adapter.calc();
        rfcommServer.poll();
    }
}

