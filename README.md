# android-rfcomm-client #

A library for simple access to a Bluetooth device with the rfcomm protocol.
User of the library have a polling access to the stream.
The library takes care of all the nasty threading.

It is designed to be used in games.

# installation #

Install on local machine:
```
./gradlew -p android-rfcomm-client installArchives
```

